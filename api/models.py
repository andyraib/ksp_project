# This is an auto-generated Django model module.
# You'll have to do the following manually to clean this up:
#   * Rearrange models' order
#   * Make sure each model has one field with primary_key=True
#   * Make sure each ForeignKey and OneToOneField has `on_delete` set to the desired behavior
#   * Remove `managed = False` lines if you wish to allow Django to create, modify, and delete the table
# Feel free to rename the models, but don't rename db_table values or field names.
from django.db import models
from django.utils import timezone
import uuid

from django.db.models.signals import post_save
from django.dispatch import receiver


from datetime import timedelta, date


SEX = (
	('femenino','FEMENINO'),
	('masculino','MASCULINO'),
)



class empleado(models.Model):
	foto = models.CharField(max_length=256)
	nombre_completo = models.CharField(max_length=256)
	puesto = models.CharField(max_length=56)
	salario = models.DecimalField(max_digits=15, decimal_places=2)
	estatus = models.CharField(max_length=56)
	fecha_contratacion = models.DateField()
	class Meta:
		managed = False
		db_table = 'empleado'

class beneficiario(models.Model):
	def get_pk ():		
		uid = uuid.uuid4()		
		new_pk = uid.hex[:5].upper()
		return new_pk

	empleado = models.ForeignKey('empleado', models.DO_NOTHING)
	nombre_completo = models.CharField(max_length=256)
	parentesco = models.CharField(max_length=256)
	fecha_nacimiento = models.DateField()
	sexo = models.CharField(max_length=15);
	
	class Meta:
		managed = False
		db_table = 'beneficiario'
