from django.shortcuts import render
from rest_framework import  status, viewsets
from rest_framework.decorators import api_view
from  api.serializers import BeneficiarioSerializer, EmpleadoSerializer
from api.models import empleado, beneficiario
from rest_framework.response import Response
from django.db.models import Count

# Create your views here.

# ViewSets define the view behavior.
class EmpleadoViewSet(viewsets.ModelViewSet):
	queryset = empleado.objects.all()
	serializer_class = EmpleadoSerializer

	def put(self, request, *args, **kwargs):
		return self.update(request, *args, **kwargs)

	def delete(self, request, id=None):
		empleadoView = empleado.objects.filter(id=id)
		empleadoView.delete()
		return Response(status=status.HTTP_204_NO_CONTENT)


class BeneficiarioViewSet(viewsets.ModelViewSet):
	queryset = beneficiario.objects.all()
	serializer_class = BeneficiarioSerializer
	
	def put(self, request, *args, **kwargs):
		return self.update(request, *args, **kwargs)
		
	def delete(self, request, id=None):
		beneficiarioView = beneficiario.objects.filter(id=id)
		beneficiarioView.delete()
		return Response(status=status.HTTP_204_NO_CONTENT)


