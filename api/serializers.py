from rest_framework import serializers
from api.models import beneficiario, empleado
			 

class EmpleadoSerializer(serializers.ModelSerializer):
	class Meta:
		model = empleado
		fields = '__all__'

class BeneficiarioSerializer(serializers.ModelSerializer):

	class Meta:
		model = beneficiario
		fields = '__all__'




