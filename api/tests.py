from django.test import TestCase
import json
from rest_framework.test import APIClient


client = APIClient()

print ('-----------------------------Get empleado------------------------------')
response = client.get('http://127.0.0.1:8000/empleado/')
assert response.status_code == 200
print (json.dumps(response.data, indent=2))
print ('-----------------------------------------------------------------------')


print ('-----------------------------Get beneficiario------------------------------')
response = client.get('http://127.0.0.1:8000/beneficiario/')
assert response.status_code == 200
print (json.dumps(response.data, indent=2))
print ('-----------------------------------------------------------------------')