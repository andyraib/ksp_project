from django.contrib import admin


from api.models import empleado, beneficiario

# Register your models here.

admin.site.register(empleado)
admin.site.register(beneficiario)
